$(document).ready(function () {
    var Scroll = function  () {
        $('html, body').stop(true, true).animate({scrollTop: $(this.hash).offset().top - navHeight + 1}, 600);
        return false;
    };
    $winWidth = $(window).width()
    nav = $(".navbar-main");
    if($winWidth > 767){
        nav.stick_in_parent({
            'parent': '.body-wrap',
            'recalc_every': 1
        });      
        navHeight = $(".navbar-main").height();
        $('.navbar-nav li a').on('click', function  () {
            $('html, body').stop(true, true).animate({scrollTop: $(this.hash).offset().top - navHeight + 1}, 600);
            return false;
        });
    }
})

$(window).bind('scroll resize', function() {
    $winWidth = $(window).width()
    if($winWidth > 767){
        var nav = $(".navbar-main");
        var currentSection = null;
        $('[data-target]').each(function(){
            var element = $(this).attr('id');   
            if ($('#'+element).is('*')){
                if($(window).scrollTop() >= $('#'+element).offset().top - nav.height())
                {
                    currentSection = element;
                }
            }
        });
        $('.navbar-nav li').removeClass('active').find('a[href="#'+currentSection+'"]').parent().addClass('active');
    }
});

$(window).load(function (argument) {  
    // Fancybox - default loading
    $(".theater").fancybox({
        helpers: {
            overlay: {
              locked: false
            }
        }
    });
    // Fancybox - load content in iframe 
    $(".ext-source").fancybox({
        'transitionIn'      : 'none',
        'transitionOut'     : 'none',
        'autoScale'         : false,
        'type'              : 'iframe',
        'width'             : '50%',
        'height'            : '60%',
        'scrolling'         : 'no'
    });
})